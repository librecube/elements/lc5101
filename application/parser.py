

def parse_si_file(file, services):

    def process_group(lines, service_group):
        d = {}
        for line in lines:
            if "END_GROUP" in line.upper():
                break
            key = line.split('=')[0]
            val = '='.join(line.split('=')[1:])
            key = key.strip()
            val = val.strip().replace(';', '').replace('"', '')
            d[key] = val
        siid = d['service-instance-id']
        del d['service-instance-id']
        service_group[siid] = d

    with open(file) as f:
        content = f.readlines()
    lines = iter(content)

    for line in lines:
        res = line.split('=')
        if len(res) == 2:
            key, val = res
            key = key.strip()
            val = val.strip()

            if key == "BEGIN_GROUP":
                if val.upper().startswith('F'):
                    process_group(lines, services['forward'])
                if val.upper().startswith('R'):
                    process_group(lines, services['return'])


def parse_proxy_config_file(file, services):

    def parse_server_types(lines):
        server_types = {}
        while True:
            line = next(lines)
            content = line.split('=')
            key = content[0].strip().upper()

            if key == 'SRV_ID':
                server_type = content[1].strip()
                server_types[server_type] = []
                line = next(lines)
                content = line.split('=')
                key = content[0].strip().upper()
                if key == 'SRV_VERSION':
                    while True:
                        line = next(lines)
                        val = line.strip()
                        if '}' in val:
                            break
                        else:
                            server_types[server_type].append(int(val))
            elif '}' in line:
                break
        return server_types

    def parse_foreign_logical_ports(lines):
        foreign_logical_ports = {}
        while True:
            line = next(lines)
            content = line.split('=')
            key = content[0].strip().upper()
            if len(content) == 2:
                val = content[1].strip()

            if key == 'PORT_NAME':
                port_name = content[1].strip()
                foreign_logical_ports[port_name] = {}
            elif key == 'PORT_DEAD_FACTOR':
                foreign_logical_ports[port_name]['deadfactor'] = val
            elif key == 'PORT_HEARTBEAT_TIMER':
                foreign_logical_ports[port_name]['heartbeat'] = val
            elif key == 'IP_ADDRESS':
                line = next(lines)
                val = line.strip()
                foreign_logical_ports[port_name]['ip_address'] = val
                while True:  # skip IP addresses of backup units
                    line = next(lines)
                    if '}' in line:
                        break
            elif '}' in line:
                break
        return foreign_logical_ports

    def parse_remote_peers(lines):
        remote_peers = {}
        while True:
            line = next(lines)
            content = line.split('=')
            key = content[0].strip().upper()
            if len(content) == 2:
                val = content[1].strip()

            if key == 'ID':
                remote_peer = val
                remote_peers[remote_peer] = {}
            elif key == 'AUTHENTICATION_MODE':
                remote_peers[remote_peer]['authentication_mode'] = val.lower()
            elif key == 'PASSWORD':
                remote_peers[remote_peer]['password'] = val
            elif '}' in line:
                break
        return remote_peers

    with open(file) as f:
        content = f.readlines()
    lines = iter(content)

    for line in lines:
        content = line.split('=')
        if len(content) == 2:
            key = content[0].strip().upper()
            val = content[1].strip()

            if key == 'LOCAL_ID':
                services['config']['local']['id'] = val
            elif key == 'LOCAL_PASSWORD':
                services['config']['local']['password'] = val

            elif key == 'SERVER_TYPES':
                services['config']['server_types'] = parse_server_types(lines)

            elif key == 'FOREIGN_LOGICAL_PORTS':
                services['config']['foreign_logical_ports'] =\
                    parse_foreign_logical_ports(lines)

            elif key == 'REMOTE_PEERS':
                services['config']['remote_peers'] = parse_remote_peers(lines)

import json
import time

import wx
import zmq
import sle

from .parser import parse_si_file, parse_proxy_config_file


ID_LOAD_PROXY_CONFIG = wx.NewIdRef()
ID_LOAD_SI = wx.NewIdRef()
ID_SHOW_CONFIG = wx.NewIdRef()


class Application(wx.Frame):

    def __init__(self):
        super().__init__(None, title="SLE User Gateway", size=(800, 600))

        # service config template
        self.services = {
            'config': {
                'local': {
                    'id': None,
                    'password': None,
                },
                'server_types': {
                    'RAF': None,
                    'RCF': None,
                    'CLTU': None,
                    'ROCF': None,
                },
                'foreign_logical_ports': {},
                'remote_peers': {}
            },
            'forward': {},
            'return': {},
        }

        self.init_ui()

    def init_ui(self):

        # Menubar

        self.menubar = wx.MenuBar()

        file_menu = wx.Menu()

        newitem = wx.MenuItem(
            file_menu, ID_LOAD_PROXY_CONFIG,
            text="Load Proxy Config", kind=wx.ITEM_NORMAL)
        file_menu.Append(newitem)

        newitem = wx.MenuItem(
            file_menu, ID_LOAD_SI,
            text="Load Service Instances", kind=wx.ITEM_NORMAL)
        file_menu.Append(newitem)

        file_menu.AppendSeparator()

        newitem = wx.MenuItem(
            file_menu, ID_SHOW_CONFIG,
            text="Show Configuration", kind=wx.ITEM_NORMAL)
        file_menu.Append(newitem)

        file_menu.AppendSeparator()

        quit = wx.MenuItem(file_menu, wx.ID_EXIT, '&Quit\tCtrl+Q')
        file_menu.Append(quit)

        self.menubar.Append(file_menu, '&File')

        self.Bind(
            wx.EVT_MENU, self.on_load_proxy_config, id=ID_LOAD_PROXY_CONFIG)
        self.Bind(wx.EVT_MENU, self.on_load_service_instances, id=ID_LOAD_SI)
        self.Bind(wx.EVT_MENU, self.on_show_configuration, id=ID_SHOW_CONFIG)
        self.Bind(wx.EVT_MENU, self.on_quit, id=wx.ID_EXIT)

        self.SetMenuBar(self.menubar)

        self.menubar.Enable(ID_LOAD_SI, False)

        # Panel - Forward Link

        panel = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)
        panel.SetSizer(vbox)

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(panel, label='Forward Link:')
        hbox.Add(label, flag=wx.RIGHT, border=8)
        vbox.Add(
            hbox, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)

        vbox.Add((-1, 10))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.forward_services_list = wx.ListCtrl(panel, style=wx.LC_REPORT)
        self.forward_services_list.InsertColumn(0, 'SIID', width=500)
        self.forward_services_list.InsertColumn(1, 'Status')
        self.forward_services_list.InsertColumn(2, 'Port')

        hbox.Add(self.forward_services_list, proportion=1, flag=wx.EXPAND)
        vbox.Add(
            hbox, proportion=1,
            flag=wx.LEFT | wx.RIGHT | wx.EXPAND, border=10)

        vbox.Add((-1, 25))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.Add(hbox, flag=wx.ALIGN_RIGHT | wx.RIGHT, border=10)

        btn = wx.Button(panel, label='OPEN')
        btn.Bind(wx.EVT_BUTTON, self.on_button_forward_open)
        hbox.Add(btn)

        btn = wx.Button(panel, label='CLOSE')
        btn.Bind(wx.EVT_BUTTON, self.on_button_forward_close)
        hbox.Add(btn, flag=wx.LEFT | wx.BOTTOM)

        btn = wx.Button(panel, label='ABORT')
        btn.Bind(wx.EVT_BUTTON, self.on_button_forward_abort)
        hbox.Add(btn, flag=wx.LEFT | wx.BOTTOM)

        # Panel - Return Link

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        label = wx.StaticText(panel, label='Return Link:')
        hbox.Add(label, flag=wx.RIGHT, border=8)
        vbox.Add(
            hbox, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)

        vbox.Add((-1, 10))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.return_services_list = wx.ListCtrl(panel, style=wx.LC_REPORT)
        self.return_services_list.InsertColumn(0, 'SIID', width=500)
        self.return_services_list.InsertColumn(1, 'Status')
        self.return_services_list.InsertColumn(2, 'Port')

        hbox.Add(self.return_services_list, proportion=1, flag=wx.EXPAND)
        vbox.Add(
            hbox, proportion=1,
            flag=wx.LEFT | wx.RIGHT | wx.EXPAND, border=10)

        vbox.Add((-1, 25))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.Add(hbox, flag=wx.ALIGN_RIGHT | wx.RIGHT, border=10)

        btn = wx.Button(panel, label='OPEN')
        btn.Bind(wx.EVT_BUTTON, self.on_button_return_open)
        hbox.Add(btn)

        btn = wx.Button(panel, label='CLOSE')
        btn.Bind(wx.EVT_BUTTON, self.on_button_return_close)
        hbox.Add(btn, flag=wx.LEFT | wx.BOTTOM)

        btn = wx.Button(panel, label='ABORT')
        btn.Bind(wx.EVT_BUTTON, self.on_button_return_abort)
        hbox.Add(btn, flag=wx.LEFT | wx.BOTTOM)

        # Statusbar

        status_bar = wx.StatusBar(self)
        self.SetStatusBar(status_bar)

    def on_load_proxy_config(self, event):
        dlg = wx.FileDialog(
            self, "Open Proxy Config", "", "",
            "Proxy Config files (*.txt)|*.txt")
        if dlg.ShowModal() == wx.ID_CANCEL:
            return
        parse_proxy_config_file(dlg.GetPath(), self.services)
        dlg.Destroy()

        self.menubar.Enable(ID_LOAD_SI, True)

    def on_load_service_instances(self, event):
        dlg = wx.FileDialog(
            self, "Open SI File", "", "", "SI files (*.si)|*.si")
        if dlg.ShowModal() == wx.ID_CANCEL:
            return
        parse_si_file(dlg.GetPath(), self.services)
        dlg.Destroy()

        self.instantiate_services()
        self.update_service_status_display()

    def on_show_configuration(self, event):
        message = json.dumps(self.services['config'], indent=4)
        dlg = wx.MessageDialog(self, message, "Show Configuration")
        dlg.ShowModal()
        dlg.Destroy()

    def on_quit(self, event):
        self.Destroy()

    def _get_service_instance(self, service_list, type):
        item = service_list.GetFirstSelected()
        if item >= 0:
            siid = service_list.GetItemText(item, 0)
            service = self.services[type].get(siid)
            return service
            if service:
                return service
                service['instance'].bind()
            self.update_service_status_display()

    def _service_open(self, service):

        if service and service['instance'].state == sle.State.UNBOUND:

            service['instance'].bind()
            self.update_service_status_display()
            wx.Yield()

            while service['instance'].state == sle.State.BIND_PEND:
                time.sleep(0.5)
                wx.Yield()
            self.update_service_status_display()
            wx.Yield()

            if service['instance'].state != sle.State.BOUND:
                return

            if 'permitted-vcids' in service:
                x = service['permitted-vcids'].split('.')[0]
                x = x[1:-1]  # remove brackets
                x = x.split(',')  # split into list
                gvcid = tuple(int(x) for x in x)
                service['instance'].start(gvcid)
            else:
                service['instance'].start()
            self.update_service_status_display()
            wx.Yield()

            while service['instance'].state == sle.State.START_PEND:
                time.sleep(0.5)
                wx.Yield()
            self.update_service_status_display()

    def _service_close(self, service):

        if service and service['instance'].state == sle.State.ACTIVE:

            service['instance'].stop()
            self.update_service_status_display()
            wx.Yield()

            while service['instance'].state == sle.State.STOP_PEND:
                time.sleep(0.5)
                wx.Yield()
            self.update_service_status_display()
            wx.Yield()

            if service['instance'].state != sle.State.BOUND:
                return

            service['instance'].unbind()
            self.update_service_status_display()
            wx.Yield()

            while service['instance'].state == sle.State.UNBIND_PEND:
                time.sleep(0.5)
                wx.Yield()
            self.update_service_status_display()

    def _service_abort(self, service):

        if service and service['instance'].state != sle.State.UNBOUND:

            service['instance'].peer_abort()
            self.update_service_status_display()
            wx.Yield()

            while service['instance'].state != sle.State.UNBOUND:
                time.sleep(0.5)
                wx.Yield()
            self.update_service_status_display()

    def on_button_forward_open(self, event):
        service = self._get_service_instance(
            self.forward_services_list, 'forward')
        self._service_open(service)

    def on_button_forward_close(self, event):
        service = self._get_service_instance(
            self.forward_services_list, 'forward')
        self._service_close(service)

    def on_button_forward_abort(self, event):
        service = self._get_service_instance(
            self.forward_services_list, 'forward')
        self._service_abort(service)

    def on_button_return_open(self, event):
        service = self._get_service_instance(
            self.return_services_list, 'return')
        self._service_open(service)

    def on_button_return_close(self, event):
        service = self._get_service_instance(
            self.return_services_list, 'return')
        self._service_close(service)

    def on_button_return_abort(self, event):
        service = self._get_service_instance(
            self.return_services_list, 'return')
        self._service_abort(service)

    def instantiate_services(self):
        forward_services = self.services.get('forward')
        return_services = self.services.get('return')

        for siid, param in forward_services.items():
            ip_address = self.services['config']['foreign_logical_ports'][
                param['responder-port-id']]['ip_address']
            host, port = ip_address.split(':')
            local_password = self.services['config']['local']['password']
            peer_password = self.services['config']['remote_peers'][
                param['responder-id']]['password']
            authentication_mode = self.services['config']['remote_peers'][
                param['responder-id']]['authentication_mode']
            version_number = max(
                self.services['config']['server_types']['CLTU'])

            if 'cltu' in siid.lower():
                instance = sle.CltuUser(
                    service_instance_identifier=siid,
                    responder_host=host,
                    responder_port=int(port),
                    responder_port_identifier=param['responder-port-id'],
                    auth_level=authentication_mode,
                    local_identifier=param['initiator-id'],
                    peer_identifier=param['responder-id'],
                    local_password=local_password,
                    peer_password=peer_password,
                    version_number=version_number)
            else:
                raise RuntimeError

            forward_services[siid]['instance'] = instance
            port_id = forward_services[siid]['responder-port-id']
            port = int(self.services['config']['foreign_logical_ports'][
                port_id]['ip_address'].split(':')[-1])
            forward_services[siid]['port'] = port
            socket = ZmqSocket.get_socket(port, 'forward')
            forward_services[siid]['socket'] = socket

        for siid, param in return_services.items():
            ip_address = self.services['config']['foreign_logical_ports'][
                param['responder-port-id']]['ip_address']
            host, port = ip_address.split(':')
            local_password = self.services['config']['local']['password']
            peer_password = self.services['config']['remote_peers'][
                param['responder-id']]['password']
            authentication_mode = self.services['config']['remote_peers'][
                param['responder-id']]['authentication_mode']
            version_number = max(
                self.services['config']['server_types']['RAF'])

            if 'raf' in siid.lower():
                instance = sle.RafUser(
                    service_instance_identifier=siid,
                    responder_host=host,
                    responder_port=int(port),
                    responder_port_identifier=param['responder-port-id'],
                    auth_level=authentication_mode,
                    local_identifier=param['initiator-id'],
                    peer_identifier=param['responder-id'],
                    local_password=local_password,
                    peer_password=peer_password,
                    version_number=version_number)
                virtual_channel = None

            elif 'rcf' or 'vcf' in siid.lower():
                instance = sle.RcfUser(
                    service_instance_identifier=siid,
                    responder_host=host,
                    responder_port=int(port),
                    responder_port_identifier=param['responder-port-id'],
                    auth_level=authentication_mode,
                    local_identifier=param['initiator-id'],
                    peer_identifier=param['responder-id'],
                    local_password=local_password,
                    peer_password=peer_password,
                    version_number=version_number)
                virtual_channel = int(
                    param['permitted-vcids'].split('.')[0][1:-1].split(',')[2])

            else:
                raise RuntimeError

            return_services[siid]['instance'] = instance
            port_id = return_services[siid]['responder-port-id']
            port = int(self.services['config']['foreign_logical_ports'][
                port_id]['ip_address'].split(':')[-1])
            return_services[siid]['port'] = port
            socket = ZmqSocket.get_socket(port, 'return')
            return_services[siid]['socket'] = socket

            frame_router = FrameRouter(socket, virtual_channel)
            instance.frame_handler = frame_router.route

    def update_service_status_display(self):
        forward_services = self.services.get('forward')
        return_services = self.services.get('return')

        idx = 0
        self.forward_services_list.DeleteAllItems()
        for siid, param in forward_services.items():
            index = self.forward_services_list.InsertItem(idx, siid)
            status = forward_services[siid]['instance'].state
            self.forward_services_list.SetItem(index, 1, status)
            port = str(forward_services[siid]['port'])
            self.forward_services_list.SetItem(index, 2, port)
            idx += 1

        idx = 0
        self.return_services_list.DeleteAllItems()
        for siid, param in return_services.items():
            index = self.return_services_list.InsertItem(idx, siid)
            status = return_services[siid]['instance'].state
            self.return_services_list.SetItem(index, 1, status)
            port = str(return_services[siid]['port'])
            self.return_services_list.SetItem(index, 2, port)
            idx += 1


class FrameRouter:

    def __init__(self, socket, virtual_channel=None):
        self.socket = socket
        self.virtual_channel = virtual_channel

    def route(self, frame):
        frame = frame.prettyPrint()
        if self.virtual_channel is None:
            data = b"99:%s" % bytes(frame, 'utf-8')
        else:
            data = b"%d: %s" % (self.virtual_channel, bytes(frame, 'utf-8'))
        self.socket.send(data)


class ZmqSocket:

    sockets = {}
    context = zmq.Context()

    @classmethod
    def get_socket(cls, port, direction):
        if port not in cls.sockets:
            if direction == 'return':
                socket = cls.context.socket(zmq.PUB)
                socket.bind("tcp://*:%s" % port)
                cls.sockets[port] = socket
            else:
                # TODO: change comm pattern to PAIR ?
                socket = cls.context.socket(zmq.PUB)
                socket.bind("tcp://*:%s" % port)
                cls.sockets[port] = socket
        else:
            socket = cls.sockets[port]
        return socket

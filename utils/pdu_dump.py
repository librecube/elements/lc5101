import sys
import zmq


if len(sys.argv) < 2:
    print("ERROR: Please provide a port and a VC (99 for RAF, blank for all)!")
    exit()

port = sys.argv[1]

if len(sys.argv) == 3:
    vc = sys.argv[2]
    topic = b"%s" % bytes(vc, 'utf-8')
else:
    topic = b""

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect("tcp://localhost:{}".format(port))
socket.setsockopt(zmq.SUBSCRIBE, topic)

while True:
    print(socket.recv())

# LibreNetwork - Space Link Extension - SLE User Gateway

This LibreCube element is a basic SLE User Gateway GUI application that allows
SLE User side to connect to SLE provider side SLE links, for the forward and
return services.

## Getting Started

Clone or download repository and change into the directory. Then run:

```bash
  virtualenv venv
  source venv/bin/activate
  pip install -r requirements.txt
```

## Running the application

Start the application:

```bash
  python run.py
```

Then in the *File* menu, first load your *Proxy Config* file and then your
*Service Instance* file. Templates for both are in the *utils* folder.

The proxy config defines the credentials and ports, whereas the service instance
file defines the SLE links that the provider offers.

Once those files are loaded, the forward and return link services are listed
on the display. To connect/disconnect the links, use the *OPEN* and *CLOSE* buttons.

When a return link is connected, the received frames are routed towards a
ZMQ socket using the publish-subscriber pattern. The port used is shown on the
display. Other applications can subscribe to this port to receive the frames.

There is a utility to show the received frames (called PDU - protocol data units)
located in the *utils* folder.

Simply start it eg. for port 5111 and virtual channel 0 like so:

```bash
  python pdu_dump.py 5111 0
```

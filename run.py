import logging; logging.basicConfig(level=logging.DEBUG)
import wx

from application import Application


if __name__ == "__main__":
    app = wx.App(False)
    gui = Application()
    gui.Show(True)
    app.MainLoop()
